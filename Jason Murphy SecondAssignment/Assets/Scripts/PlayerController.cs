﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // toggle to change between alternate movement types
    public Toggle movementToggle;
    // Works with toggle to determine if a change has just occured 
    private bool ControlChange;
    // player movement component to handle transforms 
    PlayerMovement pm;
    // player shoot component to handle shooting 
    PlayerShoot ps;
  

   public void onChangeControlType()
    {
        ControlChange = true;
    }

    // Use this for initialization
    void Start()
    {      
        pm = GetComponent<PlayerMovement>();
        ps = GetComponent<PlayerShoot>();
    }

    /// <summary>
    /// Get accelerometer input
    /// </summary>
    /// <returns></returns>
    Vector3 getInput()
    {
        // return acclertation vector
        return new Vector3(Input.acceleration.x, Input.acceleration.y, Input.acceleration.z);
    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        // get touch count for thrust and shooting

        int touchCount = Input.touchCount;

        // shoot if two touches are detected

        if (touchCount == 2)
        {
            ps.ShootContinous();
        }

        // pass inputs over to movement component to apply changes
        pm.movment(getInput(), touchCount, movementToggle.isOn, ControlChange);

        // set control change to nothing has changes for next iteration
        ControlChange = false;
    }
}
