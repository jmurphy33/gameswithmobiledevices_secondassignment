﻿using UnityEngine;
using System.Collections;
public class PlayerMovement : MonoBehaviour
{
    // thrust speed
    private float thrustValue = 5;
    // up and down speed
    private float upValue = 7f;
    // rotation speed to work with acclerometer
    float maxRotationalSpeed = 7f;
    // velocity to move player by
    private Vector3 velocity;
    // air resistance to slow player down
    private float airResistanceFactor = 0.1f;

    /// <summary>
    /// Movement to be applied to the player
    /// This includes pitching, rolling, thrusting,
    /// and turning
    /// </summary>
    /// <param name="input"></param>
    /// <param name="thrust"></param>
    /// <param name="extraControls"></param>
    /// <param name="controlChange"></param>
    public void movment(Vector3 input, int thrust, bool extraControls, bool controlChange)
    {
        if (controlChange)
        {
            // if the controlls have changed reset the rotation to 0,0,0
            // this is in the case of the player being mid turn/upside down
            transform.rotation = Quaternion.identity;
        }

        // set the velocity back to 0,0,0
        velocity = Vector3.zero;

        // if extra controls are toggled off
        if (!extraControls)
        {
            // use a faster speed for these controls
            maxRotationalSpeed = 10f;
            // apply input passed in to look left or right
            // this is done by turning the device on it's left or right side
            LookLeftRight(input);
            // apply input pased in to accend or decend the player in height
            // this is done by rotating the device facing backwards or forwards
            AdjustHeight(input);
        }
        else if (extraControls)
        {
            // slower rotation for flight mode
            maxRotationalSpeed = 3f;
            // apply pitch from passed in input. This will move the
            // player on the x axis. 
            // this is done by rotating the device facing backwards or forwards
            Pitch(input);
            // apply roll from passed in input.
            // This is done by turning the device left or right
            Roll(input);
        }

        // if touch input was one apply thrust to the player
        if (thrust == 1)
        {
            Thrust();
        }      
    }

    /// <summary>
    /// Move the player forward using touch
    /// One touch is needed to move the player forward
    /// </summary>
    void Thrust()
    {
        // increase velocity using the forward of the player and the thrust speed
        velocity += transform.forward * thrustValue;
        // move the position of the player
        transform.position += velocity * Time.deltaTime;
    }

    /// <summary>
    /// Moves the player up or down in height
    /// Rotating the device up and down
    /// </summary>
    /// <param name="input"></param>
    void AdjustHeight(Vector3 input)
    {
        transform.Translate(0, -input.y * 5 * Time.deltaTime, 0);
    }

    /// <summary>
    /// Rotates the player on the y axis
    /// Rotating the device left or right
    /// </summary>
    /// <param name="input"></param>
    public void LookLeftRight(Vector3 input)
    {
        transform.Rotate(0,input.x * maxRotationalSpeed, 0);
    }

    /// <summary>
    /// Rotates the player up or down
    /// Rotating the device up or down
    /// </summary>
    /// <param name="input"></param>
    public void Pitch(Vector3 input)
    {
        transform.Rotate(input.y * maxRotationalSpeed, 0, 0);
    }

    /// <summary>
    /// Rotates the player left or right
    /// Rotate the device left or right
    /// </summary>
    /// <param name="input"></param>
    public void Roll(Vector3 input)
    {
        transform.Rotate(0, 0, -input.x * maxRotationalSpeed);
    }
}
