﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerShoot : MonoBehaviour
{
    // UI 
    public Text HitText;
    // integers for UI text to keep track of hits and misses
    int Hits = 0, Misses = 0; 
    // Shooting
    public float rpm, distance, nextPossibleShootTime, secondsBetweenShots;
    // effect for bullets
    private LineRenderer lr;
    // Audio for bullets
    AudioSource audio;
    // layer mask for raycast 
    public LayerMask mask;
    // spawn for line renderer
    public Transform spawn;
    // access to the target manager to get the selected object
    TargetManager tm;
    // type of gun being used to determine fire mode
    public enum GunTpye
    {
        semi,
        auto,
        burst
    };
    // setting the gun type
    public GunTpye gunType;


    // Use this for initialization
    void Start()
    {
        // gets the target manager 
        tm = GameObject.FindGameObjectWithTag("TargetManager").GetComponent<TargetManager>();
       
        // gets the audio component 
        audio = GetComponent<AudioSource>();
        
        // sets the time inbetween each shoot fired
        secondsBetweenShots = 60 / rpm;

        // gets the line renderer component 
        lr = GetComponentInChildren<LineRenderer>();   
    }

    /// <summary>
    /// Fires a raycast and resolves the outcome
    /// </summary>
    public void Shoot()
    {
        if (CanShoot())
        {
            bool shotLanded = false;
            // standard raycast to detect colision with certain object
            // ray cast is sent from front of spawn area.
            Ray ray = new Ray(spawn.position, spawn.forward);

            // to keep track of the hit
            RaycastHit hit;

            // the distance the player can shoot         
            distance = 100;

            if (Physics.Raycast(ray, out hit, distance, mask))
            {
                // change the layer mask from selected to nontarget
                tm.change(tm.SelectedObject, "NonTarget", Color.red, true);
                shotLanded = true;                                  
            }
           
            // delay before can shoot again
            nextPossibleShootTime = Time.time + secondsBetweenShots;

            // play audio for shooting
            audio.Play();

            // update the text on the GUI for hits and misses
            UpdateText(shotLanded);

            // start the coroutine for the line tracer to simulate shooting
            StartCoroutine("RenderTracer");

        }
    }

    /// <summary>
    /// Updates the GUI text
    /// </summary>
    private void UpdateText( bool shotLanded)
    {  
        if (shotLanded)
        {
            // if shot landed update hit total
            Hits++;
        }
        else
        {
            // if no shot landed update miss total
            Misses++;
        }
        // update the text on the GUI to keep track of hits and misses
        HitText.text = Hits + " Hits\n" + Misses + " Misses";

    }

    /// <summary>
    /// Corouting method to simulare bullets using a 
    /// line renderer 
    /// </summary>
    /// <returns></returns>
    IEnumerator RenderTracer()
    {
        // turn on the line renderer for a certain amount of time
        // before turing it off
        lr.enabled = true;
        yield return new WaitForSeconds(.019f); ;
        lr.enabled = false;
    }

    /// <summary>
    /// Time test to check if the player can shoot again
    /// </summary>
    /// <returns></returns>
    private bool CanShoot()
    {
        // set to true and if the time for the
        // next possible chance to fire hasn't happened 
        // set to false

        bool canShoot = true;

        if (Time.time < nextPossibleShootTime)
        {
            canShoot = false;
        }


        return canShoot;
    }


    /// <summary>
    /// A check to see if the player is using an automatic
    /// semi and burst weren't implemented.
    /// </summary>
    public void ShootContinous()
    {
        if (gunType == GunTpye.auto)
        {
            Shoot();
        }
    }
}
