﻿using UnityEngine;
using System.Collections;

public class TargetManager : MonoBehaviour
{
    // array of targets
    public GameObject[] targets;
    // selected object that will be shot at
    public GameObject SelectedObject;

    // Use this for initialization
    void Start()
    {
        // set array of targets
        targets = GameObject.FindGameObjectsWithTag("Targets");
    }

    /// <summary>
    /// find a random target and return it
    /// </summary>
    /// <returns></returns>
    GameObject findObject()
    {
        // get random number
        int i = Random.Range(0, targets.Length);
        // change colour and layer for object in array
        change(targets[i], "Selected",Color.green,false);
        // return object found
        return targets[i];
    }

    
    /// <summary>
    /// Updates the selected 
    /// </summary>
    /// <param name="g"></param>
    /// <param name="name"></param>
    /// <param name="c"></param>
    /// <param name="setNull"></param>
    public void change(GameObject g,string name, Color c, bool setNull)
    {
        // change layer
        g.layer = LayerMask.NameToLayer(name);
        // change colour
        g.GetComponent<Renderer>().material.color = c;

        // done when an object is shot at
        if (setNull)
        {
            // change to null so update can reassign object
            SelectedObject = null;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // if there are is no selected object, find one
        if (SelectedObject == null)
        {
            SelectedObject = findObject();
        }

    }



}

