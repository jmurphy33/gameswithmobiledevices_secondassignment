﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// Controls the camera functionality
/// </summary>
public class CameraController : MonoBehaviour
{
   
    // tims to smooth camera movement 
    public float smoothTime = 0.3f;
    GameObject player;
    private Vector3 velocity = Vector3.zero;
    bool isCameraMovement;
    
    // location to reset to 
    private Vector3 startinglocation;
    // rotation to reset to
    private Quaternion startingRotation;
  
    // cam rotation for touch
    // touch 1 position
    private Vector3 point1;       
    // touch 2 position 
    private Vector3 point2;
    // points for x,y and z
    private float xAngle = 0.0f;
    private float yAngle = 0.0f;
    // place holders for x, y and z
    private float xAnglePlaceholder = 0.0f;
    private float yAnglePlaceholder = 0.0f;

    
    Vector3 offset;

    void Start()
    {
        // set start location to reset to
        startinglocation = transform.position;
        // set start rotation to reset to
        startingRotation = transform.rotation;
        // find and set the player
        player = GameObject.FindGameObjectWithTag("Player");
        // set the offset for the camera
        offset = player.transform.position - transform.position;
    }

    void Update()
    {
        // if is player moving the scene
        if (!isCameraMovement)
        {       
            // follow the player with the camera 
            float desiredAngle = player.transform.eulerAngles.y;
            Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
            transform.position = player.transform.position - (rotation * offset);
            transform.LookAt(player.transform);
            
        }
        else
        {
            if (Input.touchCount == 1)
            {
                // if one touch do rotations of camera
                CamRotation();
            }

            if (Input.touchCount == 2)
            {
                // if two touch do movement of camera
                CamMove();
            }
        }
    }

    /// <summary>
    /// When the toggle is selected to 
    /// use camera movment or use player movment
    /// </summary>
    public void OnCameraMovementClick()
    {
        // if not using camera movment 
        if (!isCameraMovement)
        {
            // start using camera movement
            isCameraMovement = true;
            // disable to player
            player.SetActive(false);
        }
        else
        {
            // if using camera movement already, switch to false
            isCameraMovement = false;
            // re-enable the player
            player.SetActive(true);
            // reset the rotations and positions of the camera
            transform.position = startinglocation;
            transform.rotation = startingRotation;
        }

    }

    /// <summary>
    /// Move the camera using 1 touch
    /// </summary>
    void CamMove()
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Moved:
                // get delta position of touch 0 by a speed and delta time
                Vector2 touchDeltaPos = Input.GetTouch(0).deltaPosition * 1 * Time.deltaTime;
                // move camera on the x and y using the delta postion
                Camera.main.transform.Translate(touchDeltaPos.x, touchDeltaPos.y, 0);
                break;
        }
    }

    /// <summary>
    /// Rotate the camera using 2 touch
    /// </summary>
    void CamRotation()
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                // get point 1 for touch 0 position
                point1 = Input.GetTouch(0).position;
                // get placeholders for x and y axis
                xAnglePlaceholder = xAngle;
                yAnglePlaceholder = yAngle;
                break;

            case TouchPhase.Moved:
                // get point 2 for touch 1 position
                point2 = Input.GetTouch(0).position;

                // determine x and y using the placeholders of the previous positions 
                // +/- the x and ys of point 1 and 2 multiplided by the screen height/width
                xAngle = xAnglePlaceholder + (point2.x - point1.x) * 180.0f / Screen.width;
                yAngle = yAnglePlaceholder - (point2.y - point1.y) * 90.0f / Screen.height;

                // rotate using new x and y found and 0 for z
                Camera.main.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
                break;
        }
    }

}